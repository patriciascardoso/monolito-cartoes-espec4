package br.com.monilito.Cartoes.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "cartoes")
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int numero;

    @Column(columnDefinition = "boolean default false")
    private boolean ativo;

    @OneToOne
    private Cliente cliente;

    @OneToMany(mappedBy = "cartao")
    private List<Pagamento> pagamentos;


    public Cartao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
