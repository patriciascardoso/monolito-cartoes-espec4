package br.com.monilito.Cartoes.services;

import br.com.monilito.Cartoes.models.Cliente;
import br.com.monilito.Cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Optional;


@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClienteId (int id){
            Optional<Cliente> clientes = clienteRepository.findById(id);

            if(clientes.isPresent()){
              return clientes.get();

            }else {
                throw new RuntimeException("O Cliente não foi encontrado");
            }
        }
    }

