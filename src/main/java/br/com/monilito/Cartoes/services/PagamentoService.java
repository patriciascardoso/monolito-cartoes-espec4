package br.com.monilito.Cartoes.services;

import br.com.monilito.Cartoes.DTOs.PagamentoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaPagamentoDTO;
import br.com.monilito.Cartoes.models.Cartao;
import br.com.monilito.Cartoes.models.Pagamento;
import br.com.monilito.Cartoes.repositories.PagamentoRepository;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private CartaoService cartaoService;


    public RespostaPagamentoDTO criarPagamento(PagamentoDTO pagamentoDTO){
        Cartao cartaoDB = cartaoService.buscarCartaoPorId(pagamentoDTO.getCartao_id());

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());
        pagamento.setCartao(cartaoDB);
        pagamentoRepository.save(pagamento);

        RespostaPagamentoDTO pagamentoCadastradoDTO = new RespostaPagamentoDTO();
        pagamentoCadastradoDTO.setId(pagamento.getId());
        pagamentoCadastradoDTO.setCartao_id(pagamento.getCartao().getId());
        pagamentoCadastradoDTO.setDescricao(pagamento.getDescricao());
        pagamentoCadastradoDTO.setValor(pagamento.getValor());

        return pagamentoCadastradoDTO;
    }
}
