package br.com.monilito.Cartoes.services;

import br.com.monilito.Cartoes.DTOs.AtivarCartaoDTO;
import br.com.monilito.Cartoes.DTOs.CadastrarCartaoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaCadastroCartaoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaCartaoNumeroDTO;
import br.com.monilito.Cartoes.models.Cartao;
import br.com.monilito.Cartoes.models.Cliente;
import br.com.monilito.Cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private  CartaoRepository cartaoRepository;

    @Autowired
   private ClienteService clienteService;



    public RespostaCadastroCartaoDTO cadastrarCartao (CadastrarCartaoDTO cadastrarCartaoDTO) {
        Cliente cliente = clienteService.buscarClienteId(cadastrarCartaoDTO.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cadastrarCartaoDTO.getNumero());
        cartao.setCliente(cliente);

        cartaoRepository.save(cartao);

        RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = new RespostaCadastroCartaoDTO();
        respostaCadastroCartaoDTO.setAtivo(Boolean.FALSE);
        respostaCadastroCartaoDTO.setClienteId(cartao.getCliente().getId());
        respostaCadastroCartaoDTO.setNumero(cartao.getNumero());
        respostaCadastroCartaoDTO.setId(cartao.getId());

        return respostaCadastroCartaoDTO;
    }

    private Cartao buscarCartao(int numeroCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findByNumero(numeroCartao);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public RespostaCartaoNumeroDTO buscarCartaoNumero(int numero){
        Cartao cartao = this.buscarCartao(numero);

        RespostaCartaoNumeroDTO respostaCartaoNumeroDTO = new RespostaCartaoNumeroDTO();
        respostaCartaoNumeroDTO.setId(cartao.getId());
        respostaCartaoNumeroDTO.setNumero(cartao.getNumero());
        respostaCartaoNumeroDTO.setClienteId(cartao.getId());

        return respostaCartaoNumeroDTO;
    }


    public RespostaCadastroCartaoDTO alterarStatusCartao (int numero, AtivarCartaoDTO cartao){
        Cartao cartaoDB = buscarCartao(numero);
        cartaoDB.setAtivo(cartao.getAtivo());


        Cartao cartaoAlterado = cartaoRepository.save(cartaoDB);
        RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = new RespostaCadastroCartaoDTO();
        respostaCadastroCartaoDTO.setId(cartaoAlterado.getId());
        respostaCadastroCartaoDTO.setNumero(cartaoAlterado.getNumero());
        respostaCadastroCartaoDTO.setAtivo(cartaoAlterado.isAtivo());
        respostaCadastroCartaoDTO.setClienteId(cartaoAlterado.getId());

        return respostaCadastroCartaoDTO;
    }

    public Cartao buscarCartaoPorId(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);

        if(optionalCartao.isPresent()){
            return optionalCartao.get();
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }


}
