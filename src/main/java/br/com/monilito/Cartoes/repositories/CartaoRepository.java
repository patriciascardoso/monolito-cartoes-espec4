package br.com.monilito.Cartoes.repositories;

import br.com.monilito.Cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository <Cartao, Integer> {

    Optional <Cartao> findByNumero (int numero);

}
