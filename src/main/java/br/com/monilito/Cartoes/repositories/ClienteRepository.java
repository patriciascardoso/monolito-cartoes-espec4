package br.com.monilito.Cartoes.repositories;

import br.com.monilito.Cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository <Cliente, Integer> {
}
