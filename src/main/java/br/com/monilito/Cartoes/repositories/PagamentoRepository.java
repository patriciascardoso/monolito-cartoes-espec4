package br.com.monilito.Cartoes.repositories;

import br.com.monilito.Cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;


public interface PagamentoRepository extends CrudRepository <Pagamento, Integer> {
}
