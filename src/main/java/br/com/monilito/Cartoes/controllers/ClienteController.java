package br.com.monilito.Cartoes.controllers;

import br.com.monilito.Cartoes.models.Cliente;
import br.com.monilito.Cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping ("clientes")
public class ClienteController {


    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody Cliente cliente){
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping ("/{id}")
    public Cliente consultaClienteId(@PathVariable(name = "id")  int id){
        try{
            return clienteService.buscarClienteId(id);
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
