package br.com.monilito.Cartoes.controllers;

import br.com.monilito.Cartoes.DTOs.PagamentoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaPagamentoDTO;
import br.com.monilito.Cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping ("/pagamento")
public class PagamentoController {

 @Autowired
 private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaPagamentoDTO criarPagamento (@RequestBody PagamentoDTO pagamentoDTO){
        try {
            return pagamentoService.criarPagamento(pagamentoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
