package br.com.monilito.Cartoes.controllers;

import br.com.monilito.Cartoes.DTOs.AtivarCartaoDTO;
import br.com.monilito.Cartoes.DTOs.CadastrarCartaoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaCadastroCartaoDTO;
import br.com.monilito.Cartoes.DTOs.RespostaCartaoNumeroDTO;
import br.com.monilito.Cartoes.models.Cartao;
import br.com.monilito.Cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping ("cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaCadastroCartaoDTO cadastrarCartao(@RequestBody CadastrarCartaoDTO cadastrarCartaoDTO) {
        try {
            return cartaoService.cadastrarCartao(cadastrarCartaoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping ("/{numero}")
    public RespostaCartaoNumeroDTO consultarNumeroCartao (@PathVariable(name = "numero") int numero){
        try {
            return cartaoService.buscarCartaoNumero(numero);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public RespostaCadastroCartaoDTO alterarStatusCartao(@PathVariable(name = "numero") int numero,
                                              @RequestBody AtivarCartaoDTO cartao){
        try {
            return cartaoService.alterarStatusCartao(numero, cartao);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}