package br.com.monilito.Cartoes.DTOs;

public class AtivarCartaoDTO {

    private boolean ativo;

    public boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
