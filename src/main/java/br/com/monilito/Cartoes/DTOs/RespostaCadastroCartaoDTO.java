package br.com.monilito.Cartoes.DTOs;

import br.com.monilito.Cartoes.models.Cartao;

public class RespostaCadastroCartaoDTO {

    private int id;
    private int numero;
    private int clienteId;
    private boolean ativo;

    public RespostaCadastroCartaoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    private RespostaCadastroCartaoDTO converterRespostaCartaoDTO(Cartao cartao) {
        RespostaCadastroCartaoDTO respostaCadastroCartaoDTO = new RespostaCadastroCartaoDTO();

        respostaCadastroCartaoDTO.setId(cartao.getId());
        respostaCadastroCartaoDTO.setNumero(cartao.getNumero());
        respostaCadastroCartaoDTO.setClienteId(cartao.getCliente().getId());
        respostaCadastroCartaoDTO.setAtivo(cartao.isAtivo());
        return respostaCadastroCartaoDTO;
    }
}
