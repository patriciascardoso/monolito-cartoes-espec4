package br.com.monilito.Cartoes.DTOs;

import br.com.monilito.Cartoes.models.Pagamento;

public class RespostaPagamentoDTO {

    private int  id;
    private int cartao_id;
    private String descricao;
    private double valor;

    public RespostaPagamentoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    private RespostaPagamentoDTO converterRespostaPagamentoDTO(Pagamento pagamento) {
        RespostaPagamentoDTO respostaPagamentoDTO = new RespostaPagamentoDTO();

        respostaPagamentoDTO.setId(pagamento.getId());
        respostaPagamentoDTO.setCartao_id(pagamento.getCartao().getId());
        respostaPagamentoDTO.setDescricao(pagamento.getDescricao());
        respostaPagamentoDTO.setValor(pagamento.getValor());

        return respostaPagamentoDTO;
    }
}
