package br.com.monilito.Cartoes.DTOs;

import br.com.monilito.Cartoes.models.Pagamento;

public class PagamentoDTO {

    private int cartao_id;
    private String descricao;
    private double valor;


    public PagamentoDTO() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }


    private PagamentoDTO converterPagamentoDTO(Pagamento pagamento) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();

        pagamentoDTO.setCartao_id(pagamento.getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());
        return pagamentoDTO;
    }
}
