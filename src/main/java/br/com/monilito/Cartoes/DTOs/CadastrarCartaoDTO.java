package br.com.monilito.Cartoes.DTOs;

import br.com.monilito.Cartoes.models.Cartao;

public class CadastrarCartaoDTO {

    private int numero;

    private int clienteId;

    public CadastrarCartaoDTO() {
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public CadastrarCartaoDTO converterCartaoDTO(Cartao cartao) {
        CadastrarCartaoDTO cadastrarCartaoDTO = new CadastrarCartaoDTO();
        cadastrarCartaoDTO.setNumero(cartao.getNumero());
        cadastrarCartaoDTO.setClienteId(cartao.getCliente().getId());
        return cadastrarCartaoDTO;
    }
}
